package org.example;

import java.util.Scanner;
import java.lang.Math;

public class Calculator
{
    public static void main( String[] args ) {
        Scanner input = new Scanner(System.in);

        String titleMainMenu = "Kalkulator Penghitung Luas dan Volum";
        String[] mainMenu  = {
                "Hitung Luas Bidang",
                "Hitung Volum",
                "Tutup Aplikasi"
        };

        String titleHitungLuas = "Pilih bidang yang akan dihitung";
        String[] menuHitungLuas = {
                "persegi",
                "lingkaran",
                "segitiga",
                "persegi panjang",
                "Kembali ke menu selanjutnya"
        };

        String titleHitungVolum = "Pilih bangun yang akan dihitung";
        String[] menuHitungVolum = {
                "kubus",
                "balok",
                "tabung",
                "Kembali ke menu selanjutnya"
        };

        while (true){
            Menu(titleMainMenu, mainMenu);

            System.out.print("\nEnter your option: ");
            int chooseMenu = input.nextInt();;
            if (chooseMenu == 1){
                kalkulatorMenu(titleHitungLuas, menuHitungLuas, chooseMenu);
            }else if (chooseMenu == 2) {
                kalkulatorMenu(titleHitungVolum, menuHitungVolum, chooseMenu);
            } else if (chooseMenu == 0){
                break;
            }else{
                System.out.println("\nPilihan anda tidak ada.\nSilahkan pilih kembali.\n");
            }
        }
    }

    private static void kalkulatorMenu(String title, String[] menu, int chooseMenu){
        Scanner input = new Scanner(System.in);

        while (true){
            Menu(title, menu);

            System.out.print("\nEnter your option: ");
            int pilihan = input.nextInt();

            if (pilihan >= 1 && pilihan <= menu.length -1){
                if(chooseMenu == 1){
                    hitungLuas(menu, pilihan);
                } else if (chooseMenu == 2) {
                    hitungVolum(menu, pilihan);
                }
                break;
            }else if (pilihan == 0){
                break;
            }
            else {
                System.out.println("\nPilihan anda tidak ada.\nSilahkan pilih kembali.\n");
            }
        }
    }

    private static void Menu(String title, String[] menu){
        titleTemplate(title);
        for(int a = 0; a < menu.length; a++){
            if(a == (menu.length - 1)){
                System.out.println("0. " + menu[a]);
            }else {
                System.out.println((a+1) + ". " + menu[a]);
            }
        }
    }

    private static void titleTemplate(String title){
        System.out.println(underline(title.length()));
        System.out.println(title);
        System.out.println(underline(title.length()));
    }

    private static String underline(int len){
        String line = "";
        for(int i = 0; i < len + 3; i++){
            line += "-";
        }
        return line;
    }

    private static void hitungLuas(String[] menu, int bidang){
        Scanner input = new Scanner(System.in);

        String title = String.format("Anda memilih %s", menu[bidang-1]);
        titleTemplate(title);

        double sisi, panjang, lebar, tinggi, jariJari, alas, luas = 0;

        if(bidang == 1){
            System.out.print("Masukkan sisi: ");
            sisi = input.nextDouble();
            luas = hitungLuasPersegi(sisi);
        }
        else if (bidang == 2) {
            System.out.print("Masukkan jari-jari: ");
            jariJari = input.nextDouble();
            luas = hitungLuasLingkaran(jariJari);
        }
        else if (bidang == 3) {
            System.out.print("Masukkan alas: ");
            alas = input.nextDouble();
            System.out.print("Masukkan tinggi: ");
            tinggi = input.nextDouble();
            luas = hitungLuasSegitiga(alas, tinggi);
        }
        else if(bidang == 4){
            System.out.print("Masukkan Panjang: ");
            panjang = input.nextDouble();
            System.out.print("Masukkan Lebar: ");
            lebar = input.nextDouble();
            luas = hitungLuasPersegiPanjang(panjang, lebar);
        }

        System.out.printf("Luas dari %s adalah %.2f\n",menu[bidang-1], luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        input.next();
    }

    private static void hitungVolum(String[] menu, int bangun){
        Scanner input = new Scanner(System.in);

        String title = String.format("Anda memilih %s", menu[bangun-1]);
        titleTemplate(title);

        double sisi, panjang, lebar, tinggi, jariJari, volum = 0;

        if(bangun == 1){
            System.out.print("Masukkan sisi: ");
            sisi = input.nextDouble();
            volum = hitungVolumKubus(sisi);
        }
        else if (bangun == 2) {
            System.out.print("Masukkan panjang: ");
            panjang = input.nextDouble();
            System.out.print("Masukkan lebar: ");
            lebar = input.nextDouble();
            System.out.print("Masukkan tinggi: ");
            tinggi = input.nextDouble();
            volum = hitungVolumBalok(panjang, lebar, tinggi);
        }
        else if (bangun == 3) {
            System.out.print("Masukkan tinggi: ");
            tinggi = input.nextDouble();
            System.out.print("Masukkan jari-jari: ");
            jariJari = input.nextDouble();
            volum = hitungVolumTabung(tinggi, jariJari);
        }

        System.out.printf("Volume dari %s adalah %.2f\n",menu[bangun-1], volum);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        input.next();
    }

    private static double hitungLuasPersegi(double sisi){
        return Math.pow(sisi, 2);
    }
    private static double hitungLuasPersegiPanjang(double panjang, double lebar){
        return panjang * lebar;
    }
    private static double hitungLuasLingkaran(double jariJari){
        return Math.PI * Math.pow(jariJari, 2);
    }
    private static double hitungLuasSegitiga(double alas, double tinggi){
        return 0.5 * alas * tinggi;
    }

    private static double hitungVolumKubus(double sisi){
       return Math.pow(sisi, 3);
    }
    private static double hitungVolumBalok(double panjang, double lebar, double tinggi){
        return panjang * lebar * tinggi;
    }
    private static double hitungVolumTabung( double tinggi, double jariJari){
        return Math.PI * tinggi * Math.pow(jariJari, 2);
    }
}
